const checkBox = document.getElementById('checkbox');
const orbitsManager = document.getElementById('orbit-container');
const sliderContainer = document.getElementById('speed');
const planets=document.querySelectorAll('.planet');
const star=document.querySelector('.sun');
// const restart=document.getElementById('Reset').onclick= function(){StartRotation()}
const originalTranslateVal={
  mercury:'translate(5vw)',
  venus:'translate(8vw)',
  earth:'translate(11vw)',
  mars:'translate(14vw)',
  jupiter:'translate(19vw)',
  saturn:'translate(26vw)',
  uranus:'translate(33vw)',
  neptune:'translate(37vw)',
  moon:'translate(1.5vw)',
  sun:'rotate(0deg)'
}

function saveCheckState() {
    if (checkBox.checked) {
      orbitsManager.classList.add('showing');
      orbitsManager.classList.remove('hiding');
    }
    else {
      orbitsManager.classList.remove('showing');
      orbitsManager.classList.add('hiding');
    }
}
const planet = ['mercury', 'venus', 'earth', 'mars', 'jupiter', 'saturn', 'uranus', 'neptune'];
const names = ['--mercurySpeed', '--venusSpeed', '--earthSpeed', '--marsSpeed', '--jupiterSpeed', '--saturnSpeed', '--uranusSpeed', '--neptuneSpeed'];
const speeds = [0.02, 0.051428, 0.08348, 0.157, 1.00112, 2.41942, 7.008, 13.765714];
function getInputOfSpeed(item) {
  var slideValue = item.value;

  // Save the data ready fro next time

  // localStorage.setItem('speed', item.value);

  for(var i = 0; i < names.length; i++)
  {
    // var nameOfItem = '--' + names[i] + 'Speed';
    var value = slideValue * speeds[i];
    var value = value + 's';
    // console.log(value)
    document.querySelector('body').style.setProperty(names[i], value);
  }
}
function StopRotation(){
    planets.forEach(element => {
        element.style.animationPlayState='paused';
        star.style.animationPlayState='paused';
});
}
// function StartRotation(){
//     planets.forEach(element => {
//         element.style.animationPlayState='running';
//         star.style.animationPlayState='running';

// });
// }
function defaultPosition(){
  const names=Object.keys(originalTranslateVal);

  names.forEach(element => {
    const elem= document.querySelector(`.${element}`);
    elem.style.animation='null';
    elem.style.transform=`${originalTranslateVal[element]}`
});
}
function Reset(){
  planet.forEach(element => {
    let ele= document.querySelector(`.${element}`);
    ele.style.animation = `${element} var(--${element}Speed) infinite linear`
});
  // location.reload();
  // const [state, setState] = setState({text:'Default value of the text state'});
}